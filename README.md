Apuntes.
===================

Material mostrado en el curso organizado por semana, y enlaces a lecturas
recomendadas.

### Semana 1

* [Diapositiva](https://gitlab.com/ucem-webservice-design/apuntes/raw/69effe60988f0caed281ffd4bebf09813ce64e02/semana1/semana1PPT.pptx)

### Semana 2

* [Diapositiva v2](https://gitlab.com/ucem-webservice-design/apuntes/raw/91cd44d3c494f0c3ba62ff441749697fd8133731/semana2/semana2PPT_v2.pptx) - Se extiende la explicación de los conceptos para mejorar su completitud, y facilitar su compresión.

### Semana 3

* [Guía diseño Frontend de servicios web RESTful](https://gitlab.com/ucem-webservice-design/guia-disenio-frontend) - Elaboración en progreso

### Semana 4

* [Autorización y Autenticación](https://gitlab.com/ucem-webservice-design/guia-disenio-frontend#7-seguridad)
* [Desplegar una aplicación de Spring Boot en un contenedor externo](https://gitlab.com/ucem-webservice-design/hotel-web-api#desplegar-la-aplicacion-en-un-contenedor-externo)
* [Proyecto final: Consulta de disponibilidad de cupo](https://gitlab.com/ucem-webservice-design/hotel-web-api#servicio-de-consulta-de-disponibilidad-de-cupo)

### Semana 5 

* [Rendimiento](https://gitlab.com/ucem-webservice-design/guia-disenio-frontend/blob/master/README.md#8-rendimiento)
* [Documentar el API con Springfox (implementación de swagger 2)](https://gitlab.com/ucem-webservice-design/hotel-web-api/tree/swagger-ui)
* [Proyecto final: Servicio de reservación](https://gitlab.com/ucem-webservice-design/hotel-web-api#proyecto-final-web-service-endpoints)

### Lecturas recomendadas

* [Tesis doctoral: REST](http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm) - Roy Thomas Fielding

* [REST VS SOAP cuándo es mejor REST ](https://stormpath.com/blog/rest-vs-soap) -  Claire Hunsaker

* [ REST + JSON mejores prácticas en el diseño de REST APIs](https://stormpath.com/blog/fundamentals-rest-api-design) - Stormpath CTO Les Hazlewood

* [ REST APIs directrices básicas](https://dzone.com/articles/rest-api-basic-guidelines-design-it-right-1) - CTO of RestCase

* [ Diferencia entre URLs y URIs] (https://danielmiessler.com/study/url-uri/#gs.ia1pP_U) - Daniel Miessler

* [Guía de diseño de REST APIs](https://docs.microsoft.com/en-us/azure/best-practices-api-design) - Microsoft
